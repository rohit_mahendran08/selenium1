package week2day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElements_CW {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch the browser 
		ChromeDriver driver = new ChromeDriver();
		//load url
		driver.get("https://www.google.com/");
		//maximize the browser
		driver.manage().window().maximize();
		driver.findElementById("lst-ib").sendKeys("Rohit",Keys.ENTER);
		List<WebElement> Partial = driver.findElementsByPartialLinkText("Rohit");
		System.out.println("Available Links :" +Partial.size());
		for (WebElement eachNameLink : Partial) {
			System.out.println(eachNameLink.getText());
			
		}
		
		
	}

}
