package week2day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Webtable_cw {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch the browser 
		
		ChromeDriver driver = new ChromeDriver();
		//load url
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leafground.com/pages/table.html");
		//maximize the browser
		driver.manage().window().maximize();
				WebElement table = driver.findElementByTagName("table");
				List<WebElement> rows = table.findElements(By.tagName("tr"));
				for (int i = 1; i <rows.size(); i++) {
					List<WebElement> cols = rows.get(i).
							findElements(By.tagName("td"));
					if (cols.get(1).getText().equals("80%")) {
						cols.get(2).click();
						
					}
				}}}
