package week1_day4;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alert1 {

	public static void main(String[] args) throws InterruptedException {
		//set path
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				//launch the browser 
				ChromeDriver driver = new ChromeDriver();
				//load url
				driver.get("http://leafground.com/pages/Alert.html");
				//maximize the browser
				driver.manage().window().maximize();
				driver.findElementByXPath("//button[text()='Prompt Box']").click();
				Alert switching = driver.switchTo().alert();
				String AlerText = switching.getText();
				System.out.println("Alret Text is   "+AlerText);
				Thread.sleep(3000);
				switching.sendKeys("TechWorld");
				switching.accept();
				String text_verify = driver.findElementById("result1").getText();
				if(text_verify.contains("TechWorld")) {
					System.out.println("That is a Perfect Match");
				}else {
					System.out.println("Wrong Match");
				}
				
				
				
				
				

	}

}
