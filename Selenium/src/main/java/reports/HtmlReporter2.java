package reports;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
@Test
public class HtmlReporter2 {
	
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test ;

	public  void startResult() 
	{
		
		html = new ExtentHtmlReporter("./reports/ExtentReport1.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
		
	}
	
	public void assignTest(String testcasename, String testCaseDesc, String author, String category)
	{
		test = extent.createTest(testcasename, testCaseDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	
	public void logsteps(String status, String log)
	{
		if(status.equalsIgnoreCase("pass")) {
			test.pass(log);
		}else if (status.equalsIgnoreCase("fail")) {
			test.fail(log);
		}
	}
	
	public void endResult()
	{
		extent.flush();
	}
	
}
	
	
	