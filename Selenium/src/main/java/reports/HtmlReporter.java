package reports;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
@Test
public class HtmlReporter {

	public  void main() throws IOException {
		
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/ExtentReport1.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("Login", "Login into the application");
		test.assignAuthor("Rohit Mahendran");
		test.assignCategory("Smoke");
		test.pass("user name entered", MediaEntityBuilder.createScreenCaptureFromPath(".././snaps/dashboard").build());
		test.fail("user name is not entered", MediaEntityBuilder.createScreenCaptureFromPath(".././snaps/dashboard").build());
		extent.flush();
	}

}
