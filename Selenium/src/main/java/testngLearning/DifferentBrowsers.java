package testngLearning;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import wdMethods.SeMethod;

public class DifferentBrowsers extends ProjectMethods {
	/*@BeforeTest
	@Parameters("browser")
	public void setup(String browser) throws Exception{
		//Check if parameter passed from TestNG is 'firefox'
		if(browser.equalsIgnoreCase("firefox")){
		//create firefox instance
			System.setProperty("webdriver.firefox.marionette", ".\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		//Check if parameter passed as 'chrome'
		else if(browser.equalsIgnoreCase("chrome")){
			//set path to chromedriver.exe
			System.setProperty("webdriver.chrome.driver",".\\chromedriver.exe");
			//create chrome instance
			driver = new ChromeDriver();
		}
	}*/
	@Test
	public void loginChrome() throws InterruptedException {
		
		WebElement eleuserName = locateElement("id", "username");
		type(eleuserName, "username");
		WebElement elepassword = locateElement("id", "password");
		type(elepassword, "password");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
	}
		//driver.wait(3000);
	
}
