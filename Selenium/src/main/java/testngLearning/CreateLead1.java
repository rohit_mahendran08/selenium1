package testngLearning;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead1 extends ProjectMethods{
	@Test(dataProvider="getdata")
	public void createLead(String comname, String firstname, String lastname) {
	
	WebElement CLClick = locateElement("linktext", "Create Lead");
	click(CLClick);
	WebElement Cname = locateElement("id", "createLeadForm_companyName");
	type(Cname, comname);
	WebElement FName = locateElement("id", "createLeadForm_firstName");
	type(FName, firstname);
	WebElement LName = locateElement("id", "createLeadForm_lastName");
	type(LName, lastname);
	WebElement sumbit = locateElement("xpath", "//input[@class='smallSubmit']");
	click(sumbit);
}
	@DataProvider(name="getdata")
	public Object[][] fetchData(){
		Object[][] data = new Object[2][3];
				data[0][0] = "TechMahindra";
				data[0][1]="Rohit";
				data[0][2]= "M";
				
				data[1][0]="TechMahindra";
				data[1][1]="Raj";
				data[1][2]= "R";
				
				
			return data;
		
		
	}
}