package testngLearning;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethod;

public class EditLead extends SeMethod{

	@Test
	public void edit() throws InterruptedException {
		startApp("chrome", "http://leafTaps.com/opentaps");
		
		WebElement eleuserName = locateElement("id", "username");
		type(eleuserName, "DemoCSR");
		WebElement elepassword = locateElement("id", "password");
		type(elepassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		//driver.wait(3000);
		WebElement createlead = locateElement("linktext", "CRM/SFA");
		click(createlead);
		WebElement CLClick = locateElement("linktext", "Create Lead");
		click(CLClick);
		WebElement Cname = locateElement("id", "createLeadForm_companyName");
		type(Cname, "TechMahindra");
		WebElement FName = locateElement("id", "createLeadForm_firstName");
		type(FName, "Roh");
		WebElement LName = locateElement("id", "createLeadForm_lastName");
		type(LName, "Mahe");
		WebElement sumbit = locateElement("xpath", "//input[@class='smallSubmit']");
		click(sumbit);
	}

}