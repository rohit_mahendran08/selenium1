package testcases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomcarManual {

	public static void main(String[] args) throws InterruptedException {
		//set path
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch the browser 
		ChromeDriver driver = new ChromeDriver();
		//load url
		driver.get("https://www.zoomcar.com/chennai/");
		//maximize the browser
		driver.manage().window().maximize();
		WebElement findElementByLinkText = driver.findElementByLinkText("Start your wonderful journey");
		findElementByLinkText.click();
		WebElement popular_pickup = driver.findElementByXPath("//div[@class='items'][3]");
		popular_pickup.click();
		WebElement next = driver.findElementByXPath("//button[@class='proceed']");
		next.click();
		WebElement wed = driver.findElementByXPath("//div[@class='day'][1]");
		String firsttext = wed.getText();
		System.out.println(firsttext);
		wed.click();
		WebElement next1 = driver.findElementByXPath("//button[@class='proceed']");
		next1.click();
		Thread.sleep(2000);
		WebElement confirmWed = driver.findElementByXPath("//div[@class='day picked ']");
		confirmWed.getText();
		try {
			if(confirmWed.getText().equals(firsttext))
			{
				System.out.println("Exact Matching date");
			}
			else {
				System.err.println("Wrong Match");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement done = driver.findElementByXPath("//button[@class='proceed']");
		done.click();
		
		Thread.sleep(5000);
		List<Integer> numbers = new ArrayList<>();
		List<WebElement> pricelist = driver.findElementsByXPath("//div[@class='price']");
		for (WebElement eachprice : pricelist) {
			System.out.println(eachprice.getText().replaceAll("\\D", ""));
			String replaceAll = eachprice.getText().replaceAll("\\D", "");
			int parseInt = Integer.parseInt(replaceAll);
			numbers.add(parseInt);
			
		}
		Integer max = Collections.max(numbers);
		System.out.println("Highest Price is "+ max);
		WebElement carname = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]");
		System.out.println("The highest priced car is"+ carname);
		
		

	}
	

}
