package week1day5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Set_to_List {

	public static void main(String[] args) {
		List<String> ls = new ArrayList<>();
		ls.add("Rohit");
		ls.add("Vijay");
		ls.add("Anu");
		ls.add("Rohit");
		ls.add("Cooper");
		ls.add("Vijay");
		ls.add("Mahendran");
		ls.add("Vijay");
		System.out.println(ls);
	
		Set<String> set1 = new HashSet<>();
		set1.addAll(ls);
		System.out.println(set1);

	}

}
