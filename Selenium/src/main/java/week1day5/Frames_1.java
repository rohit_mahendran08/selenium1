package week1day5;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Frames_1 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch the browser 
		ChromeDriver driver = new ChromeDriver();
		//load url
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		//maximize the browser
		driver.manage().window().maximize();
		WebDriver frame_switch = driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();;
		Thread.sleep(3000);
		Alert alert_switch = driver.switchTo().alert();
		Thread.sleep(3000);
		alert_switch.sendKeys("Rohit Mahendran");
		Thread.sleep(3000);
		alert_switch.accept();
		String verify_text = driver.findElementById("demo").getText();
		if(verify_text.contains("Rohit")) {
			System.out.println("Content Matches Exactly");
	}else {
		System.out.println("Not Matching");
	}
	}
}
