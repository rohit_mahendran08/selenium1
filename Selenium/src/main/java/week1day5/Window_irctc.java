package week1day5;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class Window_irctc {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch the browser 
		ChromeDriver driver = new ChromeDriver();
		//load url
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		//maximize the browser
		driver.manage().window().maximize();
		String ParentWindow = driver.getWindowHandle();
		driver.findElementByLinkText("Contact Us").click();
		driver.close();
		Thread.sleep(3000);
		Set<String> allwindows = driver.getWindowHandles();
		List<String> ls = new ArrayList<>();
		ls.addAll(allwindows);
		driver.switchTo().window(ls.get(0));
		System.out.println(driver.getTitle());
	}

}
